﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {
	public const string audioController = "AudioController";
	public const string backgroundMusicObject = "BackgroundMusicObject";
	public const string gameController = "GameController";
	public const string platform = "Platform";
	public const string player = "Player";
	public const string wall = "Wall";
}
