﻿using UnityEngine;
using System.Collections;

public class ScorePersistence : MonoBehaviour {

	public void HighScoreSave(){
		PersistenceController.control.Load();		


		int scoreToCheck = ScoreManager.control.getScore();
		bool highscoreAltered = false;
		if(!highscoreAltered && PersistenceController.control.maxPointsHighScore < scoreToCheck){
			
			PersistenceController.control.maxPointsHighScore3 = PersistenceController.control.maxPointsHighScore2;
			PersistenceController.control.maxPointsHighScore2 = PersistenceController.control.maxPointsHighScore;
			PersistenceController.control.maxPointsHighScore = scoreToCheck;
			
			highscoreAltered = true;
		}
		
		if(!highscoreAltered && PersistenceController.control.maxPointsHighScore2 < scoreToCheck){

			PersistenceController.control.maxPointsHighScore3 = PersistenceController.control.maxPointsHighScore2;
			PersistenceController.control.maxPointsHighScore2 = scoreToCheck;
			
			highscoreAltered = true;
		}
		
		if(!highscoreAltered && PersistenceController.control.maxPointsHighScore3 < scoreToCheck){
			
			PersistenceController.control.maxPointsHighScore3 = scoreToCheck;
			highscoreAltered = true;
		}


		PersistenceController.control.Save();
	}
}
