﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollViewObjectsInstantiator : MonoBehaviour {

	public GameObject objectToInstatiate;
	public RectTransform gapRect;
	public Transform objectsParent;
	public int numberOfObjects = 3;
	public bool gapOnStart = false;
	public bool gapOnEnd = false;
	float objectHeight = 0f;
	float gapHeight = 0f;
	float totalTopPosition = 0f;
	List <GameObject> objectsInstantiated = new List<GameObject>();

	public virtual void Start () {
		objectHeight = objectToInstatiate.GetComponent<RectTransform> ().rect.height;
		gapHeight = gapRect.rect.height;

		for (int i = 0; i < numberOfObjects; i++) {
			CreateObjects ();
		}

		for (int i = 0; i < objectsInstantiated.Count; i++) {
			RectTransform rectTransform = objectsInstantiated [i].GetComponent<RectTransform> ();

			if (i == 0) {
				totalTopPosition += objectHeight / 2;
				if (gapOnStart) {
					totalTopPosition += gapHeight;
				}
			}

			Vector2 newPos = rectTransform.anchoredPosition;
			newPos.y = -totalTopPosition;

			rectTransform.anchoredPosition = newPos;

			totalTopPosition = totalTopPosition + objectHeight + gapHeight;
		}

		RectTransform rectParent = objectsParent.GetComponent<RectTransform> ();
		Vector2 newSizeDeltaParent = rectParent.sizeDelta;
		if (!gapOnEnd) {
			totalTopPosition -= gapHeight;
		}
		newSizeDeltaParent.y = totalTopPosition - objectHeight/2;
		rectParent.sizeDelta = newSizeDeltaParent;

		objectToInstatiate.SetActive (false);
	}
	
	public virtual void CreateObjects(){ 
		objectsInstantiated.Add(Instantiate (objectToInstatiate, objectsParent));
	}

	public virtual List<GameObject> getObjectsInstantiated(){
		return objectsInstantiated;
	}
}
