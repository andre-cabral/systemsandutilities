﻿using UnityEngine;
using System.Collections;

public class ShowAdWithTimer : MonoBehaviour {

	public float timeToShow = 5.9f;
	float timeToShowPassed = 0f;

	void Update () {
		if (timeToShowPassed < timeToShow) {
			timeToShowPassed += Time.deltaTime;
		}else{
			UnityAdsManager.ShowSimpleVideoAd ();
		}
	}
}
