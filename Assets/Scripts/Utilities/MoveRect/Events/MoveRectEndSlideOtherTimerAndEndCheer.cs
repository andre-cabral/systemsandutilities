﻿using UnityEngine;
using System.Collections;

public class MoveRectEndSlideOtherTimerAndEndCheer : MoveRectReachEndEvent {

	public bool endCheer = true;
	public MusicGameController musicGameController;
	public AudienceController audienceController;
	public float timeToStartEvents = 0f;
	float timeToStartEventsPassed = 0f;
	public MoveRect[] moveToStart;
	bool eventsActivated = false;
	public MoveRect[] moveToEnd;
	public float timeToEnd = 0f;

	void Update(){
		if (eventsActivated) {
			if (timeToStartEventsPassed < timeToStartEvents) {
				timeToStartEventsPassed += Time.deltaTime;
			} else {
				EventsAction ();
				if (endCheer && audienceController != null ) {
					audienceController.EndCheerAudience ();
				}
			}
		}
	}

	public override void ExecuteEvents(){
		eventsActivated = true;
	}

	void EventsAction(){
		eventsActivated = false;
		timeToStartEventsPassed = 0f;

		if (!musicGameController.getEndGame ()) {
			foreach (MoveRect moveRect in moveToStart) {
				moveRect.GoToStart ();
			}
			foreach (MoveRect moveRect in moveToEnd) {
				moveRect.GoToEnd ();
			}
		}
	}
}
