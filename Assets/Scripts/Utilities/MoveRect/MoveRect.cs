﻿using UnityEngine;
using System.Collections;

public class MoveRect : MonoBehaviour {

	public float moveX = 0.4f;
	public float moveY = 0f;
	public float speed = 5f;
	public MoveRectReachStartEvent reachStartEvent;
	public MoveRectReachEndEvent reachEndEvent;
	public float deltaTimeWhenPaused = 0.01f;
	public bool blockMoveOnChanging = false;
	float timePassed = 0f;
	RectTransform origin;
	Vector2 startMinAnchor;
	Vector2 startMaxAnchor;
	Vector2 endMinAnchor;
	Vector2 endMaxAnchor;

	bool changing = false;
	bool toStartPosition = true;

	void Awake () {
		origin = GetComponent<RectTransform>();
		startMinAnchor = origin.anchorMin;
		startMaxAnchor = origin.anchorMax;

		endMinAnchor = startMinAnchor;
		endMinAnchor.x += moveX;
		endMinAnchor.y += moveY;

		endMaxAnchor = startMaxAnchor;
		endMaxAnchor.x += moveX;
		endMaxAnchor.y += moveY;
	}
	
	// Update is called once per frame
	void Update () {
		if(changing){
			if(!toStartPosition){
				if(!V2Equal(origin.anchorMin, endMinAnchor) && !V2Equal(origin.anchorMax, endMaxAnchor)){
					float moved = 0f;
					if(Time.deltaTime > 0){
						moved = Time.deltaTime * speed;
					}else{
						moved = deltaTimeWhenPaused * speed;
					}
					origin.anchorMin = Vector2.Lerp(origin.anchorMin, endMinAnchor, moved);
					origin.anchorMax = Vector2.Lerp(origin.anchorMax, endMaxAnchor, moved);
					timePassed += moved;
				}else{
					changing = false;
					timePassed = 0f;
					if(reachEndEvent != null){reachEndEvent.ExecuteEvents();}
				}
			}else{
				if(!V2Equal(origin.anchorMin, startMinAnchor) && !V2Equal(origin.anchorMax, startMaxAnchor)){
					float moved = 0f;
					if(Time.deltaTime > 0){
						moved = Time.deltaTime * speed;
					}else{
						moved = deltaTimeWhenPaused * speed;
					}
					origin.anchorMin = Vector2.Lerp(origin.anchorMin, startMinAnchor, moved);
					origin.anchorMax = Vector2.Lerp(origin.anchorMax, startMaxAnchor, moved);
					timePassed += moved;
				}else{
					changing = false;
					timePassed = 0f;
					if(reachStartEvent != null){reachStartEvent.ExecuteEvents();}
				}
			}
		}
	}

	public void GoToStart(){
		if(!changing || !blockMoveOnChanging){
			timePassed = 0f;
			changing = true;
			toStartPosition = true;
		}
	}

	public void GoToEnd(){
		if(!changing || !blockMoveOnChanging){
			timePassed = 0f;
			changing = true;
			toStartPosition = false;
		}
	}

	public void StopTheMovement(){
		changing = false;
	}

	public void ToggleMovement(){
		timePassed = 0f;
		changing = true;
		toStartPosition = !toStartPosition;
	}

	public void ToggleMovementOnlyWhenStopped(){
		if(!changing){
			timePassed = 0f;
			changing = true;
			toStartPosition = !toStartPosition;
		}
	}

	public bool V2Equal(Vector2 a, Vector2 b){
		return Vector2.SqrMagnitude(a - b) < 0.0001;
	}

	public bool getChanging(){
		return changing;
	}

	public bool getToStartPosition(){
		return toStartPosition;
	}
}
