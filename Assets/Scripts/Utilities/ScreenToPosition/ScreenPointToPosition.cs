﻿using UnityEngine;
using System.Collections;

public class ScreenPointToPosition : MonoBehaviour {
	public static Vector3 GetWorldPosition(Vector3 screenPoint, float distance){
		Ray ray = Camera.main.ScreenPointToRay(screenPoint);
		Vector3 point = ray.origin + (ray.direction * distance);
		return point;
	}
}
