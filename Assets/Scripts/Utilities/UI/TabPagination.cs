﻿using UnityEngine;
using System.Collections;

public class TabPagination : MonoBehaviour {

	public ImageUISpriteSwap thisTab;
	public ImageUISpriteSwap[] otherTabs;
	public GameObject thisPage;
	public GameObject[] otherPages;

	public void Clicked(){
		thisTab.SwapToSprite (1);
		for (int i = 0; i < otherTabs.Length; i++) {
			otherTabs [i].SwapToSprite (0);
		}

		thisPage.SetActive (true);
		for (int i = 0; i < otherPages.Length; i++) {
			otherPages [i].SetActive (false);
		}
	}
}
