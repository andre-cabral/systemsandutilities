﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapManager : MonoBehaviour {

	public TextAsset[] csvFiles;
	public TypeOfMap typeOfMap;
	public GameObject[] objectsToInstantiate;
	List<string>[,] grid;
	List<GameObject>[,] objectsOnPosition;
	int linesLength = 0;
	int columnsLength = 0;

	void Awake(){
		
		string[] linesStart = Lines (csvFiles[0].text);
		linesLength = linesStart.Length;
		columnsLength = Columns (linesStart [0]).Length;

		grid = new List<string>[columnsLength,linesLength];
		objectsOnPosition = new List<GameObject>[columnsLength, linesLength];

		for(int count=0;count<csvFiles.Length;count++){
			string[] lines = Lines (csvFiles[count].text);

			for (int i = 0; i < linesLength; i++) {
				string[] column = Columns (lines [i]);
				if (column.Length > 1) {
					for (int j = 0; j < columnsLength; j++) {
						if (grid [j, i] == null) {
							grid [j, i] = new List<string> ();
						}
						if(objectsOnPosition[j, i] == null){
							objectsOnPosition [j, i] = new List<GameObject> ();
						}
						grid [j, i].Add (column [j]);
					}
				}
			}
		}

		if (typeOfMap == TypeOfMap.CenterOfScreen2d) {
			CenterOfScreen2d ();
		}
		if (typeOfMap == TypeOfMap.StartOnPointZero2d) {
			StartOnPointZero2d ();
		}
	}

	void CenterOfScreen2d(){
		float centerX = columnsLength / 2;
		float centerY = linesLength / 2;
		for (int i = 0; i < linesLength; i++) {
			for (int j = 0; j < columnsLength; j++) {
				for (int k = 0; k < grid [j, i].Count; k++) {
					int objectIndex = int.Parse (grid[j, i][k]);
					if (objectIndex >= 0 && objectIndex < objectsToInstantiate.Length) {
						GameObject instantiatedObj = (GameObject)Instantiate (objectsToInstantiate [objectIndex], new Vector3 (j - centerX, (linesLength - i) - centerY, 0), Quaternion.identity);
						instantiatedObj.name = (j).ToString () + "," + (i).ToString ();
						instantiatedObj.transform.SetParent (transform);
						objectsOnPosition [j, i].Add (instantiatedObj);

						MapObject mapObj = instantiatedObj.GetComponent<MapObject> ();
						if (mapObj != null) {
							mapObj.setPosX (j);
							mapObj.setPosY (i);
							mapObj.setMaxX (columnsLength);
							mapObj.setMaxY (linesLength);
							mapObj.setMapManager (this);
						}
					}
				}
			}
		}
	}

	void StartOnPointZero2d(){
		for (int i = 0; i < linesLength; i++) {
			for (int j = 0; j < columnsLength; j++) {
				for (int k = 0; k < grid [j, i].Count; k++) {
					int objectIndex = int.Parse (grid[j, i][k]);
					if (objectIndex >= 0 && objectIndex < objectsToInstantiate.Length) {
						GameObject instantiatedObj = (GameObject)Instantiate (objectsToInstantiate [objectIndex], new Vector3 (j, (linesLength - i), 0), Quaternion.identity);
						instantiatedObj.name = (j).ToString () + "," + (i).ToString ();
						instantiatedObj.transform.SetParent (transform);
						objectsOnPosition [j, i].Add (instantiatedObj);

						MapObject mapObj = instantiatedObj.GetComponent<MapObject> ();
						if (mapObj != null) {
							mapObj.setPosX (j);
							mapObj.setPosY (i);
							mapObj.setMaxX (columnsLength);
							mapObj.setMaxY (linesLength);
							mapObj.setMapManager (this);
						}
					}
				}
			}
		}
	}

	string[] Lines(string text){
		string[] splitted = text.Split ('\n');

		if(splitted[splitted.Length-1] == ""){
			
			string[] newSplitted = new string[splitted.Length - 1];
			for (int i = 0; i < newSplitted.Length; i++) {
				newSplitted [i] = splitted [i];
			}
			splitted = newSplitted;
		}

		return splitted;
	}

	string[] Columns(string text){
		return text.Split (',');
	}

	public List<string>[,] getGrid(){
		return grid;
	}

	public int getMapHeight(){
		return linesLength;
	}

	public int getMapWidth(){
		return columnsLength;
	}

	public List<GameObject> GetObjectsOnPosition(int posX, int posY){
		return objectsOnPosition [posX, posY];
	}

	public void ChangeObjectPosition(GameObject objectToChange, int oldX, int oldY, int newX, int newY){
		objectsOnPosition [oldX, oldY].Remove (objectToChange);
		objectsOnPosition [newX, newY].Add (objectToChange);
	}

	public void RemoveObject(GameObject objectToRemove, int posX, int posY){
		objectsOnPosition [posX, posY].Remove (objectToRemove);
	}
}

public enum TypeOfMap{
	CenterOfScreen2d,
	StartOnPointZero2d
}