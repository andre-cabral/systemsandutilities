﻿using UnityEngine;
using System.Collections;

using System;

public class WindowsPhone8AdMob : MonoBehaviour {
#if UNITY_WP8
	public static WindowsPhone8AdMob control;

	//NOT WORKING
	//public static event Action<bool> MakeBannerVisible = delegate { 	};
	public static event Action ShowInterstitial = delegate { 	};

	void Awake() {

		//DontDestroyOnLoad make the object available even when you change the scene.
		//The problem is that you must enter a scene that have the object first
		//The solution is to have one copy of the object on each scene, but when you change scenes
		//the object from previous scene is there too. So you make sure you Destroy this if there is another 
		//object from the class instanced.
		//It can be verified because the variable "control" is static and is unique for every object on the class.
		//So you can verify if any object of the class is in the control static variable.
		if(control == null){
			DontDestroyOnLoad(gameObject);
			control = this;
		}else if(control != this){
			Destroy (gameObject);
		}

	}


	//NOT WORKING
	/*
	public static void BannerVisibility(bool visibility){


		if (MakeBannerVisible != null){
			MakeBannerVisible(visibility);
		}

	}
	*/

	public static void InterstitialShowStart(){
		if (ShowInterstitial != null){
			ShowInterstitial();
		}
		
	}
#endif
}
