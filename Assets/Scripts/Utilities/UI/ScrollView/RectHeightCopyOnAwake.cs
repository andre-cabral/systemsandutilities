﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RectHeightCopyOnAwake : MonoBehaviour {

	public RectTransform rectToCopy;
	public RectTransform rectToPaste;

	void Awake () {
		Vector2 newSize = rectToPaste.sizeDelta;
		newSize.y = rectToCopy.rect.height;
		rectToPaste.sizeDelta = newSize;
	}
}
