﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	private int score = 0;
	public Text scoreNumber;
	public bool canHaveNegativeScore = false;
	public static ScoreManager control;

	void Awake(){
		if(control == null){
			control = this;
		}else{
			Destroy(gameObject);
		}
	}

	public void addScore(int scoreToAdd){
		score += scoreToAdd;
		updateScoreNumber();
	}

	public void subtractScore(int scoreToSubtract){
		score -= scoreToSubtract;
		if(score < 0 && !canHaveNegativeScore){
			score = 0;
		}
		updateScoreNumber();
	}

	void updateScoreNumber(){
		scoreNumber.text = score.ToString();
	}

	public int getScore(){
		return score;
	}
}