﻿using UnityEngine;
using System.Collections;

public class ActivateAndDeactivateTargetObjectAfterTime : MonoBehaviour {

	public GameObject targetObject;
	public float timeToActivate = 3f;
	float timeToActivatePassed = 0f;
	public ActivationEnum activateTarget;

	void Update(){
		if (timeToActivatePassed < timeToActivate) {
			timeToActivatePassed += Time.deltaTime;
		} else {
			if (activateTarget == ActivationEnum.activate) {
				ActivateTheObject ();
			} else {
				DeactivateTheObject ();
			}
		}
	}

	public void ActivateTheObject(){
		targetObject.SetActive(true);
	}
	public void DeactivateTheObject(){
		targetObject.SetActive(false);
	}
}

public enum ActivationEnum{
	activate,
	deactivate
}