﻿using UnityEngine;
using System.Collections;

public class DontMoveObjectGlobal : MonoBehaviour {
	
	Vector3 startPosition;
	
	void Awake () {
		startPosition = transform.position;
	}
	
	void LateUpdate () {
		transform.position = startPosition;
	}
}
