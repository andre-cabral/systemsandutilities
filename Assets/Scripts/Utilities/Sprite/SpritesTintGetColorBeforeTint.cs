﻿using UnityEngine;
using System.Collections;

public class SpritesTintGetColorBeforeTint : SpritesTint {
		
	public override void StartTint () {
		Color[] newStartColor = new Color[spritesRenderers.Length];
		for(int i=0; i<spritesRenderers.Length; i++){
			newStartColor[i] = spritesRenderers[i].color;
		}
		setStartColor (newStartColor);
		base.StartTint ();
	}

}
