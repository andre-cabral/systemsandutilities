﻿using UnityEngine;
using System.Collections;

public class SpritesTintForTime : MonoBehaviour {

	public SpritesTint[] spritesTints;
	public float tintTime = 0.5f;
	float tintTimePassed = 0f;
	int tintActive = 999;

	public void StartTint(int tintIndex){
		if(tintIndex < spritesTints.Length){
			tintActive = tintIndex;
			tintTimePassed = 0f;
		  	spritesTints[tintIndex].StartTint();
		}
	}

	void Update(){
		if(tintActive != 999 && tintActive < spritesTints.Length){
			if(tintTimePassed < tintTime){
				tintTimePassed += Time.deltaTime;
			}else{
				tintTimePassed = 0f;
				spritesTints[tintActive].EndTint();
				tintActive = 999;
			}
		}
	}
}
