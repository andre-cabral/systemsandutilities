﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour {

	public static GameOverManager control;
	public float timeToShowGameOver = 0.2f;
	float timeToShowGameOverPassed = 0f; 
	public GameObject gameOverObjectToShow;
	bool gameOver = false;

	void Awake(){
		if (control == null) {
			DontDestroyOnLoad (gameObject);
			control = this;
		} else {
			Destroy (gameObject);
		}
	}

	void Update () {
		if (gameOver) {
			if(timeToShowGameOverPassed < timeToShowGameOver){
				timeToShowGameOverPassed += Time.deltaTime;
			}else{
				PauseMenuController.control.pauseAndUnpause.Pause();
				gameOverObjectToShow.SetActive (true);
			}
		}
	}

	public void PlayerDied(){
		gameOver = true;
	}

	public bool getGameOver(){
		return gameOver;
	}
}
