﻿using UnityEngine;
using System.Collections;

public class SpritesTintGetAlphaBeforeTint : SpritesTint {
		
	public override void StartTint () {
		Color[] newStartColor = getStartColor();
		for(int i=0; i<spritesRenderers.Length; i++){
			newStartColor [i].a = spritesRenderers[i].color.a;
		}
		setStartColor (newStartColor);
		base.StartTint ();
	}

}
