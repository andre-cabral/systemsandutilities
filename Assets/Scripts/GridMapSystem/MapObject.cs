﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapObject : MonoBehaviour {

	MapManager mapManager;
	int posX = 0;
	int posY = 0;
	int maxX = 0;
	int maxY = 0;

	public void setMapManager(MapManager mapManager){
		this.mapManager = mapManager;
	}

	public void setPosX(int posX){
		this.posX = posX;
	}

	public void setPosY(int posY){
		this.posY = posY;
	}

	public void setMaxX(int maxX){
		this.maxX = maxX;
	}

	public void setMaxY(int maxY){
		this.maxY = maxY;
	}

	public bool CanMoveToDirection(Directions direction){
		if (direction == Directions.up) {
			return CanMoveToSquare (posX, posY - 1, direction);
		}
		if (direction == Directions.down) {
			return CanMoveToSquare(posX, posY+1,direction);
		}
		if (direction == Directions.left) {
			return CanMoveToSquare(posX-1, posY,direction);
		}
		if (direction == Directions.right) {
			return CanMoveToSquare(posX+1, posY,direction);
		}
		return false;
	}

	public bool CanMoveToSquare(int squareX, int squareY, Directions direction){
		if (squareX >= 0 && squareX < maxX && squareY >= 0 && squareY < maxY) {
			List<GameObject> objectsOnPosition = mapManager.GetObjectsOnPosition (squareX, squareY);
			if (objectsOnPosition.Count == 0) {
				return false;
			} else {
				bool returnValue = true;
				for (int i = 0; i < objectsOnPosition.Count; i++) {
					if (!ActionObjectOnSquare (objectsOnPosition [i], direction)) {
						returnValue = false;
					}
				}
				return returnValue;
			}
		} else {
			return false;
		}
	}

	public bool ActionObjectOnSquare(GameObject objectOnSquare, Directions direction){
		if (objectOnSquare.tag == "Player") {
			MapObject mapObj = objectOnSquare.GetComponent<MapObject> ();
			if (mapObj.CanMoveToDirection (direction)) {
				mapObj.MoveToDirection (direction);
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	public void MoveToDirection(Directions direction){
		if (direction == Directions.up) {
			MoveToSquare (posX, posY-1,direction);
		}
		if (direction == Directions.down) {
			MoveToSquare (posX, posY+1,direction);
		}
		if (direction == Directions.left) {
			MoveToSquare (posX-1, posY,direction);
		}
		if (direction == Directions.right) {
			MoveToSquare (posX+1, posY,direction);
		}
	}

	public void MoveToSquare(int newX, int newY, Directions direction){
		if (CanMoveToSquare (newX, newY,direction)) {
			List<GameObject> objectsOnPosition = mapManager.GetObjectsOnPosition (newX, newY);
			if (objectsOnPosition.Count > 0) {
				transform.position = objectsOnPosition [0].transform.position;
			}
			mapManager.ChangeObjectPosition (gameObject, posX, posY, newX, newY);
			posX = newX;
			posY = newY;
		}
	}


}
