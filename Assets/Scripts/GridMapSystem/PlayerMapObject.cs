﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMapObject : MapObject {

	public int maxActionsOnQueue = 1;
	int actionsToExecuteIndex = 0;
	List<PlayerActions> actionsOnQueue = new List<PlayerActions>();
	bool moved = false;

	void Update(){
		if (Input.GetAxisRaw ("Vertical") > 0 && !moved) {
			AddActionToQueue (PlayerActions.up);
			moved = true;
		}
		if (Input.GetAxisRaw ("Vertical") < 0 && !moved) {
			AddActionToQueue (PlayerActions.down);
			moved = true;
		}

		if (Input.GetAxisRaw ("Horizontal") < 0 && !moved) {
			AddActionToQueue (PlayerActions.left);
			moved = true;
		} 
		if (Input.GetAxisRaw ("Horizontal") > 0 && !moved) {
			AddActionToQueue (PlayerActions.right);
			moved = true;
		}

		if (Input.GetButtonDown ("Fire1") && !moved) {
			ExecuteNextActionOnQueue ();
		}

		if (Input.GetAxisRaw ("Horizontal") == 0 && Input.GetAxisRaw ("Vertical") == 0) {
			moved = false;
		}
	}

	public void AddActionToQueue(PlayerActions action){
		if (actionsOnQueue.Count < maxActionsOnQueue) {
			actionsOnQueue.Add (action);
		}
	}

	public void ExecuteNextActionOnQueue(){
		if (actionsToExecuteIndex < actionsOnQueue.Count) {
			if (actionsToExecuteIndex < maxActionsOnQueue) {
				ActionsList ();
				actionsToExecuteIndex++;
			}
		} else {
			actionsToExecuteIndex = 0;
			actionsOnQueue.Clear ();
		}
	}

	public void ActionsList(){
		switch (actionsOnQueue [actionsToExecuteIndex]) {
		case PlayerActions.up:
			MoveToDirection (Directions.up);
			break;
		case PlayerActions.down:
			MoveToDirection (Directions.down);
			break;
		case PlayerActions.left:
			MoveToDirection (Directions.left);
			break;
		case PlayerActions.right:
			MoveToDirection (Directions.right);
			break;

		case PlayerActions.hold:
			break;
		}
	}

}

public enum PlayerActions{
	up,
	right,
	down,
	left,
	hold
}