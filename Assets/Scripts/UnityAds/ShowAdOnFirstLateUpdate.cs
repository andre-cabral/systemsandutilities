﻿using UnityEngine;
using System.Collections;

public class ShowAdOnFirstLateUpdate : MonoBehaviour {

	bool show = false;
	void LateUpdate () {
		if (!show) {
			show = true;
			UnityAdsManager.ShowSimpleVideoAd ();
		}
	}
}
