﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public static ScoreManager control;
	public Text scoreText;
	string startingText = "";
	int score = 0;

	void Awake () {
		if (control == null) {
			control = this;
			if (control.scoreText != null) {
				control.startingText = control.scoreText.text;
				control.scoreText.text = control.startingText + control.score.ToString ();
			}
		} else {
			Destroy (gameObject);
		}
	}
	
	public void AddScore(int scoreToAdd){
		score += scoreToAdd;
		if (control.scoreText != null) {
			scoreText.text = startingText + score.ToString ();
		}
	}
}
