﻿using UnityEngine;
using System.Collections;

public class ShowInterstitial : MonoBehaviour {

	public void ShowTheInterstitial () {
#if UNITY_WP8
		WindowsPhone8AdMob.InterstitialShowStart();
#endif
#if UNITY_ANDROID || UNITY_IOS
		AndroidIosBanner.InsterstitialShow();
#endif
	}
}
