﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using System.Collections;

public class goToSceneTimerWhenNotShowinAd : MonoBehaviour {
	public string sceneName;
	public float timeToGoToScene = 1f;
	float timeToGoToScenePassed = 0f;

	public void Update(){
		if (timeToGoToScenePassed < timeToGoToScene) {
			timeToGoToScenePassed += Time.deltaTime;
		} else {
			if (!Advertisement.isShowing) {
				selectScene ();
			}
		}
	}

	public void selectScene (){
		SceneManager.LoadScene(sceneName);
	}
}
