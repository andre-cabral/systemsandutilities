﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

public class UnityAdsManager : MonoBehaviour {
	
	public static void ShowSimpleVideoAd(){
		if (Advertisement.IsReady()){
			Advertisement.Show();
		}
	}
}
