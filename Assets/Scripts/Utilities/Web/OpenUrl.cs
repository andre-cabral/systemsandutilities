﻿using UnityEngine;
using System.Collections;

public class OpenUrl : MonoBehaviour {

	public string urlToOpen;

	public void OpenTheUrl(){
		Application.OpenURL(urlToOpen);
	}
}
