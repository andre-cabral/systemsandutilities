﻿using UnityEngine;
using System.Collections;

using GoogleMobileAds.Api;

public class AndroidIosBanner : MonoBehaviour {

	public static AndroidIosBanner control;

	void Awake(){
		#if UNITY_ANDROID || UNITY_IOS
		if(control == null){
			control = this;
			RequestBanner();

			control.RequestInterstitial();

		}else{
			Destroy(gameObject);
		}
		#endif
	}

	private void RequestBanner(){
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3727480082572566/5701162337";

		//test 
		//string adUnitId = "ca-app-pub-3940256099942544/6300978111";
		#elif UNITY_IOS
		string adUnitId = "ca-app-pub-3727480082572566/1131361936";

		//test 
		//string adUnitId = "ca-app-pub-3940256099942544/6300978111";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		if(adUnitId != "unexpected_platform"){
			// Create a 320x50 banner at the top of the screen.
			BannerView bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
			// Create an empty ad request.
			AdRequest request = new AdRequest.Builder()
				//.AddTestDevice(AdRequest.TestDeviceSimulator)
				//.AddTestDevice("D6DAE9267040BDC8848F7C555A03FDC5")
				.Build();
			// Load the banner with the request.
			bannerView.LoadAd(request);
		}
	}

	public InterstitialAd interstitial;
	public bool interstitialLoaded = false;

	private void RequestInterstitial()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3727480082572566/7177895535";

		//test 
		//string adUnitId = "ca-app-pub-3940256099942544/1033173712";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-3727480082572566/2608095132";

		//test 
		//string adUnitId = "ca-app-pub-3940256099942544/1033173712";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		if(adUnitId != "unexpected_platform"){
			// Initialize an InterstitialAd.
			interstitial = new InterstitialAd(adUnitId);

			interstitial.AdLoaded += InterstitialAdLoaded;
			interstitial.AdClosed += InterstitialAdClosed;
			// Create an empty ad request.
			AdRequest request = new AdRequest.Builder()
				//.AddTestDevice(AdRequest.TestDeviceSimulator)
				//.AddTestDevice("D6DAE9267040BDC8848F7C555A03FDC5")
				.Build();
			// Load the interstitial with the request.
			interstitial.LoadAd(request);
		}
	}

	public void InterstitialAdLoaded(object sender, System.EventArgs args)
	{
		control.interstitialLoaded = true;
	}

	public void InterstitialAdClosed(object sender, System.EventArgs args)
	{
		control.interstitialLoaded = false;
		interstitial.Destroy();
		control.RequestInterstitial();
	}
	
	public static void InsterstitialShow(){

		if (control.interstitial != null && control.interstitialLoaded) {
			control.interstitial.Show();
		}

	}
}
