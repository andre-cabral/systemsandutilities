﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InputManager : MonoBehaviour {

	public InputButtonWithSlide[] buttonsWithSlide;

	void Update () {
		GetInputs ();
	}

	public void RecalculateAllButtons(float rotationZ){
		foreach (InputButtonWithSlide buttonWithSlide in buttonsWithSlide) {
			buttonWithSlide.Recalculate (rotationZ);
		}
	}

	void GetInputs(){
		#if UNITY_EDITOR || UNITY_WEBGL
		if (Input.GetMouseButton (0)) {
			AnyButtonsUnpress ();

			for (int j = 0; j < buttonsWithSlide.Length; j++) {
				if (buttonsWithSlide [j].ButtonArea.Contains (Input.mousePosition)) {
					buttonsWithSlide [j].AnyButtonPressed = true;
					if (!buttonsWithSlide [j].ButtonPressed) {
						buttonsWithSlide [j].ButtonPressed = true;
						buttonsWithSlide [j].StartPress ();
					}
				}
			}

		} else {
			AnyButtonsUnpress ();
		}
		#else
		if(Input.touchCount > 0){
		AnyButtonsUnpress();
		Touch[] touches = Input.touches;

		for(int i=0; i<touches.Length; i++ ){
			for (int j = 0; j < buttonsWithSlide.Length; j++) {
				if(buttonsWithSlide[j].ButtonArea.Contains(touches[i].position)){
				buttonsWithSlide [j].AnyButtonPressed = true;
					if(!buttonsWithSlide [j].ButtonPressed){
						buttonsWithSlide [j].ButtonPressed = true;
						buttonsWithSlide [j].StartPress ();
					}
				}
			}
		}
		}else{
			AnyButtonsUnpress();
		}
		#endif
		ReleaseUnusedButtons ();
	}

	void AnyButtonsUnpress(){
		for (int i = 0; i < buttonsWithSlide.Length; i++) {
			buttonsWithSlide [i].AnyButtonPressed = false;
		}
	}

	void ReleaseUnusedButtons(){
		for (int i = 0; i < buttonsWithSlide.Length; i++) {
			if (!buttonsWithSlide [i].AnyButtonPressed && buttonsWithSlide [i].ButtonPressed) {
				buttonsWithSlide [i].ButtonPressed = buttonsWithSlide [i].AnyButtonPressed;
				buttonsWithSlide [i].EndPress ();
			}
		}
	}

	Rect GetRectFromRectTransformUI(RectTransform rectTransform){
		return new Rect(rectTransform.anchorMin.x*Screen.width,rectTransform.anchorMin.y*Screen.height, GetWidthFromRectTransformUI(rectTransform), GetHeightFromRectTransformUI(rectTransform));
	}

	float GetWidthFromRectTransformUI(RectTransform rectTransform){
		//the rect is a percentage. the maximum number, 1, represents 100% from the screen width
		//so we multiply the screen width by the rect to get the real width of the button
		float rectWidth = rectTransform.anchorMax.x - rectTransform.anchorMin.x;
		rectWidth *= Screen.width;
		
		return rectWidth;
	}
	
	float GetHeightFromRectTransformUI(RectTransform rectTransform){
		//the rect is a percentage. the maximum number, 1, represents 100% from the screen width
		//so we multiply the screen width by the rect to get the real width of the button
		float rectHeight = rectTransform.anchorMax.y - rectTransform.anchorMin.y;
		rectHeight *= Screen.height;
		
		return rectHeight;
	}
}
